<?php
namespace App\BITM\SEIP118250\mobile;
use PDO;

class Mobile
{
    public $id = '';
    public $first_name = '';
    public $last_name = '';
    public $nick_name = '';
    public $user = 'root';
    public $pass = '';
    public $servername = 'localhost';
    public $dbname = 'lab_exam-7';
    public $conn;

    public function __construct()
    {
        try {


            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->user, $this->pass);


        } catch (PDOException $e) {
            echo 'error' . $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('first-name', $data) and !empty($data)) {
            $this->first_name = $data['first-name'];
        }
        if (array_key_exists('last-name', $data) and !empty($data)) {
            $this->last_name = $data['last-name'];
        }
        if (array_key_exists('nick-name', $data) and !empty($data)) {
            $this->nick_name = $data['nick-name'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        //var_dump($this);
        return $this;
    }


    public function store()
    {

        $query = "INSERT INTO `mobile_models` (`first_name`, `last_name`, `nick_name`) VALUES (:fn, :ln, :nn)";

        $stmt = $this->conn->prepare($query);

        $result = $stmt->execute(array(

            ':fn' => $this->first_name,
            ':ln' => $this->last_name,
            ':nn' => $this->nick_name
        ));
        if ($result) {
            header('Location:index.php');
        }

    }

    public function index()
    {
        $sqlquery = "SELECT * FROM `mobile_models`";
        $stmt = $this->conn->query($sqlquery);
        $alldata = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $alldata;
    }

    public function view()
    {
        $data = "SELECT * FROM `mobile_models` WHERE `id`=:setid";
        $stmt = $this->conn->prepare($data);
        $stmt->execute(array(
            ':setid' => $this->id
        ));
        $singledata = $stmt->fetch(PDO::FETCH_ASSOC);
        return $singledata;
    }

    public function update()
    {
        $query = "UPDATE `mobile_models` SET `first_name` = :fn, `last_name` = :ln, `nick_name` = :nn WHERE `mobile_models`.`id` = :id;";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(

            ':fn' => $this->first_name,
            ':ln' => $this->last_name,
            ':nn' => $this->nick_name,
            ':id' => $this->id
        ));

        if ($result) {
            header('Location:index.php');
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `mobile_models` WHERE `mobile_models`.`id` = :id";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(

            ':id' => $this->id
        ));

        if ($result) {
            header('Location:index.php');
        }

    }



}