<?php
namespace App\BITM\SEIP118250\Email;
use PDO;

class Email
{
    public $id='';
    public $name='';
    public $email='';
    public $user = 'root';
    public $pass = '';
    public $servername = 'localhost';
    public $dbname = 'lab_exam-7';
    public $conn;
    public function __construct()
    {
        try {


            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->user, $this->pass);


        } catch (PDOException $e) {
            echo 'error' . $e->getMessage();
        }
    }



    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('email', $data) and !empty($data)) {
            $this->email = $data['email'];
        }


         if (array_key_exists('id', $data) and !empty($data)) {
           $this->id = $data['id'];
         }
        return $this;
    }

    public function store()
    {

        $query = "INSERT INTO `Email` ( `name`, `email`) VALUES ( :name, :email)";

        $stmt = $this->conn->prepare($query);

        $result = $stmt->execute(array(

            ':name' => $this->name,
            ':email' => $this->email
        ));
        if ($result) {
            header('Location:index.php');
        }

    }

    public function index(){
        $sqlquery = "SELECT * FROM `Email`";
        $stmt = $this->conn->query($sqlquery);
        $alldata = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $alldata;
    }
    public function view()
    {
        $data = "SELECT * FROM `Email` WHERE `id`=:setid";
        $stmt = $this->conn->prepare($data);
        $stmt->execute(array(
            ':setid' => $this->id
        ));
        $singledata = $stmt->fetch(PDO::FETCH_ASSOC);
        return $singledata;
    }

    public function update(){

        $query = "UPDATE `Email` SET `name` = :name, `email` = :email, WHERE `Email`.`id` = :setid;";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(

            ':name' => $this->name,
            ':email' => $this->email,
            ':setid' => $this->id
        ));

        if ($result) {
            header('Location:index.php');
        }
    }



}