<?php

namespace App\BITM\SEIP118250\Hobby;
use PDO;
class Hobby
{
    public $id='';
    public $name='';
    public $hobby='';
    public $user = 'root';
    public $pass = '';
    public $servername = 'localhost';
    public $dbname = 'lab_exam-7';
    public $conn;
    public function __construct()

    {
        try {


            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->user, $this->pass);


        } catch (PDOException $e) {
            echo 'error' . $e->getMessage();
        }
    }
    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('hobby', $data) and !empty($data)) {
            $this->hobby = $data['hobby'];
        }


        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store()
    {

        $query = "INSERT INTO `Hobby` ( `name`, `hobby`) VALUES ( :name, :hobby)";

        $stmt = $this->conn->prepare($query);

        $result = $stmt->execute(array(

            ':name' => $this->name,
            ':hobby' => $this->hobby
        ));
        if ($result) {
            header('Location:index.php');
        }

    }
    public function index()
    {
        $sqlquery = "SELECT * FROM `hobby`";
        $stmt = $this->conn->query($sqlquery);
        $alldata = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $alldata;

    }
    public function view()
    {
        $data = "SELECT * FROM `hobby` WHERE `id`=:setid";
        $stmt = $this->conn->prepare($data);
        $stmt->execute(array(
            ':setid' => $this->id
        ));
        $singledata = $stmt->fetch(PDO::FETCH_ASSOC);
        return $singledata;
    }
    public function update(){

        $query = "UPDATE `hobby` SET `name` = :name, `hobby` = :hobby  WHERE `hobby`.`id` =:setid";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(
            ':name' => $this->name,
            ':hobby' => $this->hobby,
            ':setid' => $this->id
        ));

        if ($result) {
            header('Location:index.php');
        }
    }

}