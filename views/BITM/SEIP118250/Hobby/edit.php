

<?php
include_once("../../../../vendor/autoload.php");
use App\BITM\SEIP118250\Hobby\Hobby;
$obj= new Hobby();

$value=$obj->setData($_GET)->view();
//var_dump($value);
$stringValue= $value['hobby'];
$arrayValue= explode(",",$stringValue);
//var_dump($arrayValue);

//die();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

        <h2>Hobby</h2>

        <form action="update.php" method="post">
            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $value['id']?>">
                <label for="name">Your Name:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $value['name']?>"
                       placeholder="Enter your  name">
            </div>
            <label>Your Hobby:</label>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Football"
                  <?php
                    if(in_array('Football',$arrayValue)){
                        echo "checked";
                    }
                  ?>

                    >Football</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]"   value="Cricket"

                        <?php
                        if(in_array('Cricket',$arrayValue)){
                            echo "checked";
                        }
                        ?> >Cricket</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]"  value="Gardening"
                        <?php
                        if(in_array('Gardening',$arrayValue)){
                            echo "checked";
                        }
                        ?> >Gardening</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Ha Du Du"<?php
                    if(in_array('Ha Du Du',$arrayValue)){
                        echo "checked";
                    }
                    ?> >Ha Du Du</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="hobby[]" value="Hockey"
                        <?php
                        if(in_array('Hockey',$arrayValue)){
                            echo "checked";
                        }
                        ?> >Hockey</label>
            </div>

            <button type="submit" class="btn btn-default">update</button>
        </form>

    </div>

</body>
</html>