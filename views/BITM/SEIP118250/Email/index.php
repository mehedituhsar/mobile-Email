<?php
include_once("../../../../vendor/autoload.php");
use App\BITM\SEIP118250\Email\Email;
$obj=new Email();
$data=$obj->index();
//var_dump($data);

//die();


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">

    <a href="create.php" class="btn btn-info" role="button">CREATE AGAIN</a>
</div>

<div class="container">
    <h2>ALL MOBILE LIST</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th> Name</th>
            <th>Email</th>
            <th> ACTION</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($data as $result){
        ?>
        <tr>
            <td><?php echo $sl++?> </td>
            <td> <?php echo $result['id']?></td>
            <td> <?php echo $result['name']?></td>
            <td><?php echo $result ['email']?> </td>
            <td><a href="view.php?id=<?php echo $result['id']?>" class="btn btn-primary" role="button">VIEW</a>
                <a href="edit.php?id=<?php echo $result['id']?>" class="btn btn-success" role="button">EDIT</a>
                <form method="post" action="delete.php">

                    <button type="submit" name="id" value="<?php echo $result['id']?>">delete
                    </button>
                </form>



                <?php
                }?>
        </tbody>
    </table>
</div>

</body>
</html>


